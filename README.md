# tf-env-infosec-interview #
___An empty Terraform terraform environment for infosec/DevSecOps interviews___

This repo contains an empty master, along with an open pull request that claims to contain
code and templates that set up a new service running in AWS.

Candidates should review the pull request, potentially pushing their own changes if time 
allows, while talking the interviewer through their thought process/reasons for requesting
the changes.

